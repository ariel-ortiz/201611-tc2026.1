/* global $ */

$(function () {
  
  $('#mi-botoncito').click(function () {
    $.getJSON('/palabra')
    .done(function (data) {
      $('#mi-palabra').text(data.palabra);
      console.log('Data: ' + JSON.stringify(data));
    })
    .fail(function (err) {
      console.log('Error: ' + err);
    })
  });
});