var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;

var palabras = ['pollito', 
                'gallina',
                'lapiz',
                'pluma',
                'ventana',
                'puerta',
                'piso',
                'techo'];

router.get('/palabra', function (req, res) {
  var n = (Math.random() * palabras.length) | 0;
  res.json({status: 'ok', palabra: palabras[n]});
});
