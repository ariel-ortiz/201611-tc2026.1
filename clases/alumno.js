/* Ejemplo de uso de clases
   en ECMAScript 6.
 */
 
'use strict';

class Alumno {
  constructor(nombre, matricula) {
    this._nombre = nombre;
    this._matricula = matricula;
  }
  toString() {
    return 'Soy un alumno (' + this.nombre + ', '
      + this.matricula + ').';
  }
  get nombre() {
    return this._nombre;
  }
  set nombre(valor) {
    this._nombre = valor;
  }
  get matricula() {
    return this._matricula;
  }
}

var a1 = new Alumno('Juan', 123);
console.log(a1.toString());
console.log(a1.nombre);
a1.nombre = 'Pedro';
console.log(a1.matricula);