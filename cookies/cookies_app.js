'use strict';

const express = require("express");

const app = express();

var contador = 0;

app.get('/', (req, res) => {
  let dato = req.get('Cookie') || 'dato=0';
  let n = parseInt(dato.split('=')[1]);
  n++;
  res.set('Set-Cookie', 'dato=' + n + '; Expires=Wed, 09 Jun 2021 10:18:14 GMT; HTTPOnly');
  contador++;
  res.type('text').send('Mensajito #' + n + '\n#Número de visitas: ' + contador + '\n');
});

app.listen(process.env.PORT, () => {
  console.log('Servidor web: ' + process.env.C9_HOSTNAME);
});
