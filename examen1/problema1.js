/*
 * Coloca aquí tu matrícula y nombre.
 */

'use strict';

const net = require('net');

const server = net.createServer((s) => { 
    
  s.on('data', data => {
    var entrada = data.toString().trim();
    if (entrada === 'fin') {
      s.end('¡Adios!\n');
    } else {
      for (var i = 0; i < entrada.length; i++) {
        s.write(entrada[i] + entrada[i]);
      }
      s.write('\n');
    }
  });
  
});

server.listen(process.env.PORT, () => {
  console.log('Servidor listo escuchando en puerto: ' + process.env.PORT);
});
