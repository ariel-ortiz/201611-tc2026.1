/*
 * Coloca aquí tu matrícula y nombre.
 */

'use strict';

const net = require('net');
const EOL = '\r\n';

const mensajes = {
  'en': 'Hello world',
  'es': 'Hola mundo',
  'fr': 'Bonjour monde'
};

function creaPagina(mensaje) { 
  return new Buffer( 
    '<!DOCTYPE html>' + EOL + 
    '<html>' + EOL + 
    '<head></head>' + EOL + 
    '<body><p>' + mensaje + '</p></body>' + EOL + 
    '</html>' + EOL);
}

net.createServer(socket => {
  
  socket.on('data', data => {
    var peticion = data.toString().trim();
    var pagina = creaPagina(mensajes['en']);
    
    for (var k in mensajes) {
      if (peticion.indexOf('Accept-Language: ' + k) !== -1) {
        pagina = creaPagina(mensajes[k]);
        break;
      }
    }
    
    socket.write('HTTP/1.1 200 OK' + EOL + 
      'Server: ServidorExamen/0.1' + EOL + 
      'Date: ' + new Date().toUTCString() + '' + EOL + 
      'Content-Length: ' + pagina.length + '' + EOL + 
      'Content-Type: text/html; charset=utf-8' + EOL + 
      EOL);
    socket.end(pagina);
  });
  
}).listen(process.env.PORT, () => {
  console.log('Servidor HTTP listo escuchando en puerto: ' + process.env.PORT);
});