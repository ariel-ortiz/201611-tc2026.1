'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;

router.post('/procesa', (req, res) => {
  req.session.info = req.body.info || 'Sin info';
  res.redirect('/procesa_cont');
});

router.get('/procesa_cont', (req, res) => {
  res.render('listo', { info: req.session.info });
});
