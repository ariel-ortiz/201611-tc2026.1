/* global $ */

$(function () {
  var contador = 0;
  $('#mi-botoncito').click(function () {
    contador++;
    $('#mi-lista').append('<li>Click ' + contador +'</li>');
    $('#mi-lista').fadeToggle(1500);
  });
});