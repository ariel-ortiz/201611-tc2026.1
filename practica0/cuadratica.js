/*----------------------------------------------------------
 * Práctica 0: Ecuaciones cuadráticas
 * Fecha: 20-Enero-2016
 * Autores:
 *           A01166611 Pepper Pots
 *           A01160611 Anthony Stark
 *---------------------------------------------------------*/

'use strict';

function numRealRoots(a, b, c) {
  let d = discriminant(a, b, c);
  if (d < 0) {
    return 0;
  } else if (d === 0) {
    return 1;
  } else {
    return 2;
  }
}

function discriminant(a, b, c) {
  return b * b - 4 * a * c;
}

// Leer un número del teclado (entrada estándar).
function readNumber(prompt, cb) {
  process.stdout.write(prompt);
  process.stdin.once('data', input => {
    let num = parseFloat(input);
    if(isNaN(num)) {
      readNumber(prompt, cb);
    } else {
      cb(num);
    }
  });
}

// Obtener los valores de A, B y C del teclado y calcular 
// e imprimir el número de raíces reales.
readNumber('A = ', a => 
  readNumber('B = ', b => 
    readNumber('C = ', c => {
      process.stdin.pause();
      console.log('Number of real roots:', numRealRoots(a, b, c));
    })
  )
);
