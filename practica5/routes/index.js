var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;

router.get('/hola/:nombre?', function (req, res) {
  var nombre = req.params.nombre || 'Mundo';
  res.render('hola.ejs', 
    { 'nombre': nombre,
      'enanos': ['Thorin', 'Kili', 'Fili', 
                 'Oin', 'Bofur']
    }
  );
});
