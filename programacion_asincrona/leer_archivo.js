// Ejemplo de lectura de un archivo de forma asíncrona.
 
'use strict';

var fs = require('fs');
var contenido;

fs.readFile('leer_archivo.js', (err, data) => {
  if (err) {
    console.log('Hubo un error', err);
  } else {
    contenido = data.toString();
    console.log(contenido);
  }
});

