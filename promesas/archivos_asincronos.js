'use strict';

const fs = require('fs');

fs.readFile('uno.txt', (err, data1) => {
  if (err) {
    throw err;
  }
  fs.readFile('dos.txt', (err, data2) => {
    if (err) {
      throw err;
    }
    fs.writeFile('tres.txt', 
      data1.toString() + data2.toString(),
      (err) => {
        if (err) {
          throw err;
        }
        console.log('¡Listo!');
      }
    )
  });
});
