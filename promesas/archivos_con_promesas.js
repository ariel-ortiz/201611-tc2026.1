'use strict';

const fs = require('fs');

function promisifiedReadFile(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function promisifiedWriteFile(file, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('¡Listo!');
      }
    });
  });
}

var result;
promisifiedReadFile('uno.txt')
.then((data) => {
  result = data.toString();
  return promisifiedReadFile('dos.txt');
})
.then((data) => {
  result += data.toString();
  return promisifiedWriteFile('cuatro.txt', result);
})
.then((mssg) => {
  console.log(mssg);
})
.catch((err) => {
  console.log('Se detectó un error: ' + err);
});
