'use strict';

Promise.all([
  new Promise((resolve, reject) => {
    setTimeout(
      () => { 
        console.log('Promesa 1'); 
        resolve(1);      
      },
      1000);
  }),
  new Promise((resolve, reject) => {
    setTimeout(
      () => { 
        console.log('Promesa 2'); 
        resolve(2);      
      },
      500);
  }),
  new Promise((resolve, reject) => {
    setTimeout(
      () => { 
        console.log('Promesa 3'); 
        resolve(3);      
      },
      700);
  })
])
.then((data) => {
  console.log(data);
})
.catch((err) => {
  console.log('Hubo error: ' + err);
});