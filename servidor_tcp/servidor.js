// Un pequeño servidor de sockets TCP.

'use strict';

const net = require('net');
const todos = [];

function borraSocket(s) {
  for (var i = 0; i < todos.length; i++) {
    if (s === todos[i]) {
      todos.splice(i, 1); // Borra el socket del arreglo en el índice i.
      return;
    }
  }
}

const server = net.createServer(s => {
  todos.push(s);
  var puerto = s.remotePort;
  console.log('Conexión a nuevo cliente, puerto ' + puerto);
  
  s.on('data', data => {
    var cadena = data.toString().trim();
    console.log(puerto + ': ' + cadena);
    if (cadena === 'bye') {
      borraSocket(s);
      s.end('Hasta la bye bye!\n');
    } else {
      todos.forEach(otro => {
        if (otro !== s) {
          otro.write(puerto + ': ' + cadena + '\n');
        }
      });
      s.write('Recibido\n');
    }
  });
  
  s.on('end', () => {
    borraSocket(s);
  });
});

server.listen(process.env.PORT, () => {
  console.log('Servidor listo escuchando en puerto: ' + process.env.PORT);
});