// Primer servidor HTTP usando sockets TCP.

'use strict';

const net = require('net');

function creaPagina(nombre) { 
  return new Buffer( 
    '<!DOCTYPE html>\n' +
    '<html>\n' +
    '<head><title>Hola</title></head>\n' +
    '<body><h1>Hola ' + nombre + '</h1></body>\n' +
    '</html>\n');
}

const server = net.createServer(s => {
  
  s.on('data', data => {
    var cadena = data.toString().trim();
    var entrada = cadena.split(/\r\n/);
    var recurso = entrada[0].split(' ')[1];
    var pagina = creaPagina(recurso.substring(1));
    //console.log(entrada);
    s.write('HTTP/1.1 200 OK\r\n' +
      'Server: MicroPicoMiniServidor/0.1\r\n' +
      'Date: ' + new Date().toUTCString() + '\r\n' +
      'Content-Length: ' + pagina.length + '\r\n' +
      'Content-Type: text/html; charset=utf-8\r\n' +
      '\r\n');
    s.end(pagina);
  });
});

server.listen(process.env.PORT, () => {
  console.log('Servidor HTTP listo escuchando en puerto: ' + process.env.PORT);
});