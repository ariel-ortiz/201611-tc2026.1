'use strict';

var fs = require('fs');
var xml2js = require('xml2js');

var parser = new xml2js.Parser();

fs.readFile('movies.xml', function (err, data) {
  if (err) {
    throw err;
  } 
  parser.parseString(data, function (err, result) {
    if (err) {
      throw err;
    }
    console.log(JSON.stringify(result));
    console.log();
    console.log(result.movies.film[6].cast[1]);
    console.log('Done');
  });
});
